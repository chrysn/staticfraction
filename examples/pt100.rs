#![feature(associated_consts)]

// This example doesn't actually implement the whole PT100 example from the
// README yet; it should, though, when more is implemented.

extern crate staticfraction;

#[cfg(feature="to_fraction")]
extern crate fraction;

#[derive(Default)]
#[derive(Debug)]
struct VoltageFromADCDescription;
impl staticfraction::StaticFractionDescription<u32> for VoltageFromADCDescription {
    const NUM_OFFSET: u32 = 0;
    const NUM_FACTOR: u32 = 1;
    const DENOM: u32 = 204;
}

type VoltageFromADC = staticfraction::StaticFraction<u8, u32, VoltageFromADCDescription>;

#[derive(Default)]
#[derive(Debug)]
struct VoltageFromUserDescription;
impl staticfraction::StaticFractionDescription<u32> for VoltageFromUserDescription {
    const NUM_OFFSET: u32 = 0;
    const NUM_FACTOR: u32 = 1;
    const DENOM: u32 = 100;
}

type VoltageFromUser = staticfraction::StaticFraction<u8, u32, VoltageFromUserDescription>; // aka centivolt

fn main() {
    let u = VoltageFromADC::new_from_stored(100);
    let u_user_low = VoltageFromUser::new_from_stored(49);
    let u_user_high = VoltageFromUser::new_from_stored(50);
    println!("{:?}. 0.4 < u < 0.5: {} {}.", u, u_user_low < u, u < u_user_high);

    #[cfg(feature="to_fraction")]
    {
        let as_fr: fraction::GenericFraction<u32> = u.to_fraction();
        println!("As fraction: {}", as_fr);
        println!("As fraction: {} {}", u_user_low.to_fraction(), u_user_high.to_fraction());
    }
}
